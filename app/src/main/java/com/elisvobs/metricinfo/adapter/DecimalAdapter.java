package com.elisvobs.metricinfo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elisvobs.metricinfo.R;
import com.elisvobs.metricinfo.model.Decimal;

import java.util.List;

public class DecimalAdapter extends RecyclerView.Adapter<DecimalAdapter.ViewHolder> {

    private List<Decimal> decimals;

    public DecimalAdapter(List<Decimal> decimals) {
        this.decimals = decimals;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView unit, symbol, prefix;

        ViewHolder(View view){
            super(view);
            unit = view.findViewById(R.id.unit);
            symbol = view.findViewById(R.id.symbol);
            prefix = view.findViewById(R.id.prefix);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.units, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.unit.setText(decimals.get(position).unit);
        holder.prefix.setText(decimals.get(position).prefix);
        holder.symbol.setText(decimals.get(position).symbol);
    }

    @Override
    public int getItemCount() {
        return decimals.size();
    }
}