package com.elisvobs.metricinfo.ui.home;

import android.widget.ExpandableListView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.elisvobs.metricinfo.adapter.CustomAdapter;

import java.util.HashMap;
import java.util.List;

public class HomeViewModel extends ViewModel {

    ExpandableListView expandableListView;
    HashMap<String, List<String>> listChild;
    List<String>listHeader;
    CustomAdapter adapter;
    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}