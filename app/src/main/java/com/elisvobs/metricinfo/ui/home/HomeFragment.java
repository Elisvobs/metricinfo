package com.elisvobs.metricinfo.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.elisvobs.metricinfo.adapter.CustomAdapter;
import com.elisvobs.metricinfo.model.ExpandableListData;
import com.elisvobs.metricinfo.R;

import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    ExpandableListView expandableListView;
    HashMap<String, List<String>> listChild;
    List<String>listHeader;
    CustomAdapter adapter;
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        final ExpandableListView expandableListView = view.findViewById(R.id.expListView);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listChild = ExpandableListData.getData();
                adapter = new CustomAdapter(context, listHeader, listChild);
                expandableListView.setAdapter(adapter);
            }
        });
        return view;
    }
}