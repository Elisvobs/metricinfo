package com.elisvobs.metricinfo.model;

public class Decimal {
    public String unit;
    public String prefix;
    public String symbol;

    public Decimal(String unit, String prefix, String symbol) {
        this.unit = unit;
        this.prefix = prefix;
        this.symbol = symbol;
    }
}
