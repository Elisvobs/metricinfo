package com.elisvobs.metricinfo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListData {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> Apple = new ArrayList<String>();
        Apple.add("Iphone");
        Apple.add("Ipad");
        Apple.add("Macbook");

        List<String> Samsung = new ArrayList<String>();
        Samsung.add("Galaxy");
        Samsung.add("Note");
        Samsung.add("TV");

        expandableListDetail.put("Apple", Apple);
        expandableListDetail.put("Samsung", Samsung);
        return expandableListDetail;
    }
}