package com.elisvobs.metricinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import static java.lang.Float.parseFloat;

public class Metric extends AppCompatActivity {
    TextInputEditText text1,text2;
    Spinner spinner3,spinner2;
    SpinnerAdapter spinneradapter;
    Button value;
    float num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_info);
        setTitle("Metric Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        spinner2=(Spinner)findViewById(R.id.spinner2);
        spinner3=(Spinner)findViewById(R.id.spinner3);
        value =(Button)findViewById(R.id.value);

        text1.setText(bundle.getString("number"));
        String n=bundle.getString("number");
        num=parseFloat(n);

        ArrayList<String> list = new ArrayList<String>();
        list.add("feet");
        list.add("inch");
        list.add("meter");
        list.add("centimeter");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.notifyDataSetChanged();
        spinner2.setAdapter(dataAdapter);
        spinner3.setAdapter(dataAdapter);

        value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float no = 0;
                if ((spinner2.getSelectedItem().toString()).equals("feet")) {
                    if ((spinner3.getSelectedItem().toString()).equals("feet")) {
                        text2.setText("" + num);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("inch")) {
                        no = num * 12;
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("meter")) {
                        no = (float) (num * 0.3048);
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("centimeter")) {
                        no = (float) (num * (30.48));
                        text2.setText("" + no);
                    }

                }
                if ((spinner2.getSelectedItem().toString()).equals("inch")) {
                    if ((spinner3.getSelectedItem().toString()).equals("inch")) {
                        text2.setText("" + num);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("feet")) {
                        no = (float) (num * (0.0833333));
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("meter")) {
                        no = (float) (num * 0.0254);
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("centimeter")) {
                        no = (float) (num * (2.54));
                        text2.setText("" + no);

                    }

                }
                if ((spinner2.getSelectedItem().toString()).equals("meter")) {
                    if ((spinner3.getSelectedItem().toString()).equals("meter")) {
                        text2.setText("" + num);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("feet")) {
                        no = (float) (num * (3.28084));
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("inch")) {
                        no = (float) (num * 39.3701);
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("centimeter")) {
                        no = (float) (num * (100));
                        text2.setText("" + no);
                    }

                }
                if ((spinner2.getSelectedItem().toString()).equals("centimeter")) {
                    if ((spinner3.getSelectedItem().toString()).equals("centimeter")) {
                        text2.setText("" + num);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("feet")) {
                        no = (float) (num * (0.0328084));
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("inch")) {
                        no = (float) (num * 0.393701);
                        text2.setText("" + no);
                    }
                    if ((spinner3.getSelectedItem().toString()).equals("meter")) {
                        no = (float) (num * (0.01));
                        text2.setText("" + no);
                    }

                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}