package com.elisvobs.metricinfo;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elisvobs.metricinfo.adapter.DecimalAdapter;
import com.elisvobs.metricinfo.model.Decimal;

import java.util.ArrayList;
import java.util.List;

public class DecimalUnits extends AppCompatActivity {
    DecimalAdapter mDecimalAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.decimal);
        setTitle("Decimal Units Multiples & Divisions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showDecimals();
    }

    private void showDecimals() {
        final List<Decimal> decimals = new ArrayList<>();
        final RecyclerView mRecyclerView = findViewById(R.id.list_unit);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        decimals.add(new Decimal("Unit", "Prefix", "Symbol"));
        decimals.add(new Decimal("10", "Deca", "D"));
        decimals.add(new Decimal("10" + "\u00B2", "Hecto", "H"));
        decimals.add(new Decimal("10", "Kilo", "K"));
        decimals.add(new Decimal("10", "Mega", "M"));
        decimals.add(new Decimal("10", "Tera", "T"));
        decimals.add(new Decimal("10", "Peta", "P"));
        decimals.add(new Decimal("10", "Exa", "e"));
        decimals.add(new Decimal("10", "Deci", "d"));
        decimals.add(new Decimal("10", "Centi", "c"));
        decimals.add(new Decimal("10", "Milli", "m"));
        decimals.add(new Decimal("10", "Micro", "u"));
        decimals.add(new Decimal("10", "Nano", "n"));
        decimals.add(new Decimal("10", "Pico", "p"));
        decimals.add(new Decimal("10", "Femto", "f"));
        decimals.add(new Decimal("10", "Atto", "a"));

         mDecimalAdapter = new DecimalAdapter(decimals);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mDecimalAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDecimalAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}