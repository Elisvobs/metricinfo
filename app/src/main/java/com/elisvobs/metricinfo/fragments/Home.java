package com.elisvobs.metricinfo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.elisvobs.metricinfo.adapter.CustomAdapter;
import com.elisvobs.metricinfo.model.ExpandableListData;
import com.elisvobs.metricinfo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {

    public Home() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        Context context = view.getContext();

        final HashMap<String, List<String>> listChild = ExpandableListData.getData();
        final List<String>listHeader = new ArrayList<String>(listChild.keySet());
        final CustomAdapter adapter  = new CustomAdapter(context, listHeader, listChild);

        final ExpandableListView expandableListView = view.findViewById(R.id.expListView);
        expandableListView.setAdapter(adapter);
        return view;
    }

}