package com.elisvobs.metricinfo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elisvobs.metricinfo.R;
import com.elisvobs.metricinfo.adapter.InfoAdapter;
import com.elisvobs.metricinfo.model.Information;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Info extends Fragment {

    public Info() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info2, container, false);
        listInfo(view);
        return view;
    }

    private void listInfo(View view) {
        Context context = view.getContext();
        final List<Information> information = new ArrayList<>();
        final RecyclerView recyclerView = view.findViewById(R.id.info_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);

        information.add(new Information("Clothing"));
        information.add(new Information("Decimal Units Multiples & Divisions"));
        information.add(new Information("Paper Sizes"));
        information.add(new Information("Tyre Pressure"));
        information.add(new Information("US Measures"));

        recyclerView.setLayoutManager(layoutManager);
        InfoAdapter adapter = new InfoAdapter(information);
        recyclerView.setAdapter(adapter);
    }
}