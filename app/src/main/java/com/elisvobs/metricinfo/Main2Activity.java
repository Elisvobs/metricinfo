package com.elisvobs.metricinfo;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Main2Activity extends AppCompatActivity {

    private BottomNavigationView .OnNavigationItemSelectedListener mOnNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    int id = menuItem.getItemId();

                    if (id == R.id.nav_home) {
                        startActivity(new Intent(Main2Activity.this, HomeActivity.class));
                        finish();
                    }  else if (id == R.id.nav_convert) {
                        startActivity(new Intent(Main2Activity.this, ConverterActivity.class));
                        finish();
                    } else if (id == R.id.nav_info) {
                        startActivity(new Intent(Main2Activity.this, InfoActivity.class));
                        finish();
                    }
                    return false;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setTitle("Metric Information");

        BottomNavigationView navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}